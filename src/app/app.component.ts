import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ticketbooking';
  vehicle = "Plane";
  trainName = 'Garibhrath Pune Express ';

  availTicket = 20;

  bookTicket(){
    if(this.availTicket!=0)
    this.availTicket--;
    console.log("You have Successfully book your ticket.",this.availTicket);
  }

  cancelTicket(){
    if(this.availTicket!=20)
    this.availTicket++;
    console.log("You have Cancelled your ticket.",this.availTicket);
  }

}
